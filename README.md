# iPedido

![Diagrama de componentes](Diagrama/Component Diagram1.jpg)

# Validar CNPJ


![](Diagrama/CalculoCNPJ.jpg)

![](Diagrama/ClculoCNPJ.jpg)

- [] Terminar de implementar os testes, tanto pela cobertura como pelas regras (Casos De Falha)
- [X] Mudar as listasr para IImmutableList
- [X] Criar builder para o projeto aplicacao

Tema   | Fonte
--------- | ------
Moq HttpClient | [Mocking HttpClient & IHttpClientFactory with Moq (the easy way)](https://kagamine.dev/en/mock-httpclient-the-easy-way/?utm_source=pocket_mylist)
Moq HttpClient | [ ASP.NET Core Unit Test: How to Mock HttpClient.GetStringAsync() ](https://edi.wang/post/2021/5/11/aspnet-core-unit-test-how-to-mock-httpclientgetstringasync?utm_source=pocket_mylist)

| Fonte
|------|
|[(Microsoft)  Fazer solicitações HTTP usando IHttpClientFactory no ASP.NET Core](https://docs.microsoft.com/pt-br/aspnet/core/fundamentals/http-requests?utm_source=pocket_mylist&view=aspnetcore-5.0)
|[(Microsoft)  HttpClient Classe](https://docs.microsoft.com/pt-br/dotnet/api/system.net.http.httpclient?utm_source=pocket_mylist&view=net-5.0)
|[(YouTube) How To Use HTTP CLIENT IN ASP NET CORE Applications  Getting Started With ASP.NET Core Series](https://www.youtube.com/watch?v=bAXZx0zOeCU)
|[(Macoratti) ASP .NET Core - Acessando o banco de dados PostGreSQL no MacOS com VS ](http://www.macoratti.net/17/06/aspncore_pgsql1.htm)
|[(Microsoft) Aplicando migrações](https://docs.microsoft.com/pt-br/ef/core/managing-schemas/migrations/applying?tabs=dotnet-core-cli)
|[(Microsoft)  Injeção de dependência no ASP.NET Core](https://docs.microsoft.com/pt-br/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-5.0)
|[(Microsoft) C# de teste de unidade no .NET Core usando dotnet test e xUnit](https://docs.microsoft.com/pt-br/dotnet/core/testing/unit-testing-with-dotnet-test)
|[(Microsoft) Arquiteturas comuns de aplicativo Web](https://docs.microsoft.com/pt-br/dotnet/architecture/modern-web-apps-azure/common-web-application-architectures)
