using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Domain.Vendedores;
using Microsoft.Extensions.Configuration;
using Moq;
using Moq.Protected;
using WebIPedido.ClienteRest;
using WebIPedido.Vendedores;
using Xunit;

namespace WebIPedido.Test
{
    public class TesteObterVendedor
    {
        
        private readonly Mock<HttpMessageHandler> _mockHttp;
        private HttpClient _httpClient;
        
        public TesteObterVendedor()
        {
            _mockHttp = _mockHttp = new Mock<HttpMessageHandler>();
            _httpClient = new HttpClient(_mockHttp.Object);
        }
        
        [Fact]
        void TestarObterVendedor()
        {
            IConfigurationRoot root = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(
                    @"{""Nome"":""Mario""}"),
            };

            NewMethod(response);
            
            IClienteRest cliente = new ClienteRestConcrete(_httpClient,root);
            
            IObterVendedor obter = new ObterVendedor(cliente);
            Assert.Equal("Mario", obter.Obter("Mario").ToString());
        }
        
        private void NewMethod(HttpResponseMessage response)
        {
            _mockHttp
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(response)
                .Verifiable();
        }
        
    }
}