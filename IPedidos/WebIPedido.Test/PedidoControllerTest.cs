using Domain.FormaPagamentos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebIPedido.FormasPagamento;
using WebIPedido.Pedidos;
using Xunit;
using FormaPagamento = WebIPedido.FormasPagamento.FormaPagamento;

namespace WebIPedido.Test
{
    public class PedidoControllerTest
    {
        [Fact]
        public void EfetuarPedido()
        {
            ILogger<PedidoController> log = new Logger<PedidoController>(new LoggerFactory());
            PedidoDBContext ctx = CriaConextoBanco("EfetuarPedido");
            SalvarFormaPagamento(ctx);
            IObterFormaPagamento obePagamento = new ObterFormaPagamento(ctx);
            PedidoController controller = new PedidoController(obePagamento, log);
            Assert.IsType<OkResult>(controller.PostPedido(new Pedido()));
        }

        private static void SalvarFormaPagamento(PedidoDBContext ctx)
        {
            var formaPagamento = new FormaPagamento();
            formaPagamento.Descricao = "A prazo";
            formaPagamento.Id = 1;

            ctx.FormasPagamento.Add(formaPagamento);
            ctx.SaveChanges();
        }

        private PedidoDBContext CriaConextoBanco(string name)
        {
            var options = new DbContextOptionsBuilder<PedidoDBContext>().UseInMemoryDatabase(databaseName: name)
                .Options;
            return new PedidoDBContext(options);
        }
    }
}