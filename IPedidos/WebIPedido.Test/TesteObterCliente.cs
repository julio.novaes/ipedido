using Domain.Clientes;
using Microsoft.EntityFrameworkCore;
using WebIPedido.Clientes;
using Xunit;

namespace WebIPedido.Test
{
    public class TesteObterCliente
    {


        [Fact(DisplayName = "Quando tentar obter um cliente retornar")]
        public void ObterClienteVelido()
        {
            var criaConextoBanco = CriaConextoBanco("ObterClienteVelido");
            
            SalvarCliente(criaConextoBanco);
            
            IObterCliente cliente = new ObterCliente(criaConextoBanco);

            var c = cliente.Obter("737.044.530-56");
            
            Assert.Equal("737.044.530-56",c.NumeroContribuinte);
        }
        
        private static void SalvarCliente(PedidoDBContext ctx)
        {
            var clienteBanco = new ClienteBanco();

            clienteBanco.Cidade = "Irece";
            clienteBanco.Nome = "Mario";
            clienteBanco.NumeroContribuinte = "737.044.530-56";
            

            ctx.Clientes.Add(clienteBanco);
            
            ctx.SaveChanges();
        }

        private PedidoDBContext CriaConextoBanco(string name)
        {
            var options = new DbContextOptionsBuilder<PedidoDBContext>().UseInMemoryDatabase(databaseName: name)
                .Options;
            return new PedidoDBContext(options);
        }
    }
}