using System.Collections.Generic;

namespace Domain.Vendedores
{
    public class MockVendedor : IObterVendedor
    {
        private List<string> ListaVendedores = new List<string>();
        public Vendedor Obter(string Nome)
        {

            return Vendedor.ComNome(ListaVendedores.Find(e => e == Nome));
        }

        public MockVendedor()
        {
            ListaVendedores.Add("Mario");
            ListaVendedores.Add("Fabio");
            ListaVendedores.Add("Jorge");
        }
    }
}