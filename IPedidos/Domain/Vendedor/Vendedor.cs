namespace Domain.Vendedores
{
    public class Vendedor 
    {
        public readonly string Nome;

        public static Vendedor ComNome(string nome) {
            return new Vendedor(nome);
        }
        private Vendedor(string nome)
        {
            this.Nome = nome;
        }

        protected bool Equals(Vendedor other)
        {
            return Nome == other.Nome;
        }

        public override string ToString()
        {
            return Nome;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Vendedor)obj);
        }

        public override int GetHashCode()
        {
            return (Nome != null ? Nome.GetHashCode() : 0);
        }

        public Vendedor Obter(IObterVendedor obter)
        {
            if (!obter.Obter(Nome).Equals(this))
                throw new VendedorException(Nome, CodigoErro.Vendedor_nao_encontrado);
            return this;
        }
    }
}