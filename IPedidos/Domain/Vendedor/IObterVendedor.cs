namespace Domain.Vendedores
{
    public interface IObterVendedor
    {
       Vendedor Obter(string Nome);
    }
}