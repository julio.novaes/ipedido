using System;
namespace Domain.Vendedores
{
    public class VendedorException : Exception
    {
        private readonly string Nome;
        private readonly CodigoErro codigoErro;

        public VendedorException(string Nome, CodigoErro CodigoErro)
        {
           this.Nome = Nome;
           this.codigoErro = CodigoErro;
        }
    }
}