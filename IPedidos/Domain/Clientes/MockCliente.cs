namespace Domain.Clientes
{
    public class MockCliente : IObterCliente
    {
        public Cliente Obter(string numeroContribuinte)
        {
            ClienteBuilder c = new ClienteBuilder();
            return c.ComNumeroContribuinte(numeroContribuinte).ComCidade("Irece").ComNome("Teste").Criar();
        }
    }
}