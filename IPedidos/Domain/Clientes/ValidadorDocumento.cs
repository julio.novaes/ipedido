using System;
using System.Collections.Generic;

namespace Domain.Clientes
{
    public abstract class ValidadorDocumento
    {
        private static int MODULO = 11;

        protected internal int basePrimeiroDigito, baseUltimoVerificador, tamanhoCadeia;

        protected internal string numeroDocumento;

        protected ValidadorDocumento(string numeroDocumento, int basePrimeiroDigito, int baseUltimoVerificador,
            int tamanhoCadeia)
        {
            this.numeroDocumento = numeroDocumento;
            this.basePrimeiroDigito = basePrimeiroDigito;
            this.baseUltimoVerificador = baseUltimoVerificador;
            this.tamanhoCadeia = tamanhoCadeia;
        }

        public bool Validar()
        {
            if (VerificarDocumentoNulo(numeroDocumento))
            {
                return false;
            }

            if (!MatchExpressao(numeroDocumento))
            {
                return false;
            }

            int[] numero = ParaInteiro(numeroDocumento);

            int digitoVerificador1 = CalcularDigitoVerificador(numero, tamanhoCadeia - 3, basePrimeiroDigito);
            if (digitoVerificador1 != numero[tamanhoCadeia - 2])
            {
                return false;
            }

            int digitoVerificador2 = CalcularDigitoVerificador(numero, tamanhoCadeia - 2, baseUltimoVerificador);
            if (digitoVerificador2 != numero[tamanhoCadeia - 1])
            {
                return false;
            }


            return true;
        }

        private bool VerificarDocumentoNulo(string numero)
        {
            return string.IsNullOrEmpty(numero);
        }

        protected abstract bool MatchExpressao(string numero);

        private int proximoFatorMultiplicador(int baseN, int fatorIncremental)
        {
            return 2 + fatorIncremental % baseN;
        }


        private int[] ParaInteiro(string numeros)
        {
            var replace = numeros.Replace(".", "").Replace("/", "").Replace("-", "").Replace(" ", "");
            List<int> lista = new List<int>();
            foreach (var numero in replace)
            {
                lista.Add(Convert.ToInt32(char.GetNumericValue(numero)));
            }

            return lista.ToArray();
        }

        private int CalcularDigitoVerificador(int[] numero, int tamanho, int baseN)
        {
            int somatorio = 0;
            for (int posicao = tamanho, fator = 0; posicao >= 0; posicao--, fator++)
            {
                int fatorN = proximoFatorMultiplicador(baseN, fator);

                somatorio += numero[posicao] * fatorN;
            }

            var calcularDigitoVerificador = (somatorio % MODULO) > 2 ? MODULO - (somatorio % MODULO) : 0;
            return calcularDigitoVerificador;
        }
    }
}