using System;
namespace Domain.Clientes
{
    public class ClienteException : Exception
    {
      private readonly string NumeroContribuinte;
      private readonly CodigoErro CodigoErro;

        public ClienteException(string NumeroContribuinte, CodigoErro CodigoErro): base( NumeroContribuinte + " - " + CodigoErro)
        {
           this.NumeroContribuinte = NumeroContribuinte;
           this.CodigoErro = CodigoErro;
        }


    }
}