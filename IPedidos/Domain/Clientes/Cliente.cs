namespace Domain.Clientes
{
    public class Cliente
    {
        public readonly string NumeroContribuinte;

        public readonly string Nome;

        public  readonly string Cidade;
        
        private FactoryDocumento _factoryDocumento;
        
        internal Cliente(string NumeroContribuinte, string Nome, string Cidade){
            this.NumeroContribuinte = NumeroContribuinte;
            this.Nome = Nome;
            this.Cidade = Cidade;
        }
        
        public Cliente Obter(IObterCliente ObterCliente)
        {
            ValidadorDocumento validador;

            validador = new FactoryDocumento().SelecionaDocumento(NumeroContribuinte);

            if (!validador.Validar()) throw new ClienteException(NumeroContribuinte,CodigoErro.NumeroContribuinteInvalido);
            
            var clienteEncontrado = ObterCliente.Obter(NumeroContribuinte);
            if (clienteEncontrado == null)
            {
                throw new ClienteException(NumeroContribuinte, CodigoErro.ClienteNaoEncontrado);
            }

            return clienteEncontrado;

        }
    }
    
}
