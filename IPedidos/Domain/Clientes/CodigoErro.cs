using System;
namespace Domain.Clientes
{
    public enum CodigoErro
    {
       ClienteNaoEncontrado = 2548404,
       NumeroContribuinteInvalido = 2548500
    }
}