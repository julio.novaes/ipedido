using System.Buffers.Text;
using System.Text.RegularExpressions;
using System.Transactions;

namespace Domain.Clientes
{
    public class CNPJ: ValidadorDocumento
    {
        private static string regexCNPJ = "^\\d{2}\\.?\\d{3}\\.?\\d{3}\\/?\\d{4}-?\\d{2}$";

        public CNPJ(string numeroDocumento) : base(numeroDocumento: numeroDocumento, tamanhoCadeia:14,basePrimeiroDigito:8,baseUltimoVerificador:8)
        {
             
        }

        protected override bool MatchExpressao(string Numero)
        {
            return Regex.IsMatch(Numero, regexCNPJ);
        }
    }
}