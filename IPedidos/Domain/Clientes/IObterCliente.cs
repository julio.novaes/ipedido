namespace Domain.Clientes
{
    public interface IObterCliente
    {
        Cliente Obter(string NumeroContribuinte);
    }
}