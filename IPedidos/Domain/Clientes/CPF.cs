using System.Text.RegularExpressions;

namespace Domain.Clientes
{
    public class CPF : ValidadorDocumento
    {
        private static string regexCPF = "^\\d{3}\\.?\\d{3}\\.?\\d{3}\\-?\\d{2}$";

        public CPF(string numeroDocumento) : base(numeroDocumento: numeroDocumento, basePrimeiroDigito: 11, baseUltimoVerificador: 12, tamanhoCadeia:11 )
        {
             
        }

        protected override bool MatchExpressao(string Numero)
        {
            return Regex.IsMatch(Numero, regexCPF);
        }
    }
}