using Domain.Clientes;

namespace Domain
{
    public class ClienteBuilder: IClienteBuilder
    {
        private string Nome,Cidade,NumeroContribuinte;

        public ClienteBuilder()
        {
            
        }
        public IClienteBuilder ComNome(string Nome)
        {
            this.Nome = Nome;
            return this;
        }
        public IClienteBuilder ComNumeroContribuinte(string NumeroContribuinte)
        {
            this.NumeroContribuinte = NumeroContribuinte;
            return this;
        }
        public IClienteBuilder ComCidade(string Cidade)
        {
            this.Cidade = Cidade;
            return this;
        }

        public Cliente Criar()
        {
            return new Cliente(NumeroContribuinte, Nome, Cidade);
        }
    }
}