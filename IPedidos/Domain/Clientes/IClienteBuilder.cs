using Domain.Clientes;

namespace Domain
{
    public interface IClienteBuilder
    {
        public IClienteBuilder ComNome(string Nome);

        public IClienteBuilder ComNumeroContribuinte(string NumeroContribuinte);

        public IClienteBuilder ComCidade(string Cidade);

        public Cliente Criar();
    }
}