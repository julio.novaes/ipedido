using System;

namespace Domain.Clientes
{
    public class FactoryDocumento
    {
        public FactoryDocumento()
        {
        }

        public ValidadorDocumento SelecionaDocumento(string numeroContribuinte)
        {
            
            ValidadorDocumento validador;

            if (numeroContribuinte == null) throw new NumeroContribuinteNuloException();
            
            var numero = numeroContribuinte
                                .Replace(".", "")
                                .Replace("/", "")
                                .Replace("-", ""); 
            if (numero.Length == 14)
            {
                validador = new CNPJ(numero);
            }
            else
            {
                validador = new CPF(numero);
            }

            return validador;
        }
    }

    public class NumeroContribuinteNuloException : Exception
    {
        public NumeroContribuinteNuloException() : base("Numero do contribuinte nao valido")
        {
            
        }
    }
}