using System;

namespace Domain.FormaPagamentos
{
    public class FormaPagamentoException : Exception
    {
        public FormaPagamentoException(string descricao, CodigoErro codigoErro) : base(descricao + "-" + codigoErro)
        {
        }
    }
}