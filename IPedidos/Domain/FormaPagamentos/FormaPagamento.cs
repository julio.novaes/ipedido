namespace Domain.FormaPagamentos
{
    public class FormaPagamento
    {
        public string Nome{get;}

        protected bool Equals(FormaPagamento other)
        {
            return Nome == other.Nome;
        }

        public override string ToString()
        {
            return Nome;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((FormaPagamento)obj);
        }

        public override int GetHashCode()
        {
            return (Nome != null ? Nome.GetHashCode() : 0);
        }

        private FormaPagamento(string nome)
        {
            this.Nome = nome;
        }

        public static FormaPagamento NovaFormaPagamento(string nome)
        {
            return new FormaPagamento(nome);
        }

        public FormaPagamento Obter(IObterFormaPagamento obter)
        {
            var formaPagamento = obter.Obter(Nome);
           
            if (formaPagamento == null)
            {
                throw new FormaPagamentoException(Nome, CodigoErro.FormaPagamentoNaoEncontrada);
            }

            if (!formaPagamento.Equals(this))
                throw new FormaPagamentoException(Nome, CodigoErro.FormaPagamentoNaoEncontrada);
            return this;
        }
        
    }
}