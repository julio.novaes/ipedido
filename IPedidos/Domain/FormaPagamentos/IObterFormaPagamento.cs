namespace Domain.FormaPagamentos
{
    public interface IObterFormaPagamento
    {
        FormaPagamento Obter(string descricao);
    }
}