using System.Collections.Generic;

namespace Domain.FormaPagamentos
{
    public class MockPagamento : IObterFormaPagamento
    {
        private List<string> formas = new List<string>();

        public MockPagamento()
        {
            formas.Add("A vista");
            formas.Add("A prazo");
            formas.Add("Cartao");
        }

        public FormaPagamento Obter(string descricao)
        {
            return FormaPagamento.NovaFormaPagamento(formas.Find(s => s == descricao));
        }
    }
}