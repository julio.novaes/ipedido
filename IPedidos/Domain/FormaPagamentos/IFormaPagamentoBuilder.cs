namespace Domain.FormaPagamentos
{
    public interface IFormaPagamentoBuilder
    {
        public IFormaPagamentoBuilder ComNome(string Nome);
        public FormaPagamento Criar();
    }
}