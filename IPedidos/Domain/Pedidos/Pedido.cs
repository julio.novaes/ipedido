using System;
using System.Collections.Immutable;
using Domain.Clientes;
using Domain.FormaPagamentos;
using Domain.Produtos;
using Domain.Vendedores;

namespace Domain.Pedidos
{
    public class Pedido
    {
        private readonly Cliente _cliente;
        private readonly Vendedor _vendedor;
        private readonly FormaPagamento _formaPagamento;
        private readonly IImmutableList<Produto> _produtos;

        internal Pedido(PedidoBuilder builder)
        {
            _cliente = builder.Cliente;
            _vendedor = builder.Vendedor;
            _formaPagamento = builder.Pagamento;
            _produtos = builder.Produtos;
        }

        public static PedidoBuilder CriarInstancia()
        {
            return new PedidoBuilder();
        }

        public void Salvar(ISalvarPedido pedido)
        {
            pedido.Salvar(this);
        }

        private bool Equals(Pedido other)
        {
            return Equals(_cliente, other._cliente) && Equals(_vendedor, other._vendedor) && Equals(_formaPagamento, other._formaPagamento) && Equals(_produtos, other._produtos);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((Pedido)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_cliente, _vendedor, _formaPagamento, _produtos);
        }
    }
}