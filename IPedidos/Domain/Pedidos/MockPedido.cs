using System.Collections.Generic;

namespace Domain.Pedidos
{
    public class MockPedido : ISalvarPedido
    {
        private ISet<Pedido> _pedidos = new SortedSet<Pedido>();
        public void Salvar(Pedido pedido)
        {
            _pedidos.Add(pedido);
        }

        public ISet<Pedido> Get()
        {
            return _pedidos;
        }
    }
}