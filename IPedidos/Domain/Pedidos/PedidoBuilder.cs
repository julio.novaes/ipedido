using System.Collections.Immutable;
using Domain.Clientes;
using Domain.FormaPagamentos;
using Domain.Produtos;
using Domain.Vendedores;

namespace Domain.Pedidos
{
    public class PedidoBuilder
    {
        internal Cliente Cliente { get; private set; }
        internal Vendedor Vendedor { get; private set; }
        internal FormaPagamento Pagamento { get; private set; }
        internal IImmutableList<Produto> Produtos { get; private set; }

        public PedidoBuilder ComCliente(Cliente cliente)
        {
            Cliente = cliente;
            return this;
        }
        
        public PedidoBuilder ComVendedor(Vendedor vendedor)
        {
            Vendedor = vendedor;
            return this;
        }
        
        public PedidoBuilder ComPagamento(FormaPagamento pagamento)
        {
            Pagamento = pagamento;
            return this;
        }
        
        public PedidoBuilder ComProdutos(IImmutableList<Produto> produtos)
        {
            Produtos = produtos;
            return this;
        }

        public Pedido Criar()
        {
            return new Pedido(this);
        }
    }
}