using System.Collections.Immutable;

namespace Domain.Produtos
{
    public interface IObterProduto
    {
        IImmutableList<Produto> Obter(IImmutableList<string> codigos);
    }
}