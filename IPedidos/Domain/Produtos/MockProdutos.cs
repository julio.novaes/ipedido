using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Domain.Produtos
{
    public class MockProdutos:IObterProduto
    {
        private List<Produto> Lista = new();
        public MockProdutos()
        {
            ProdutoBuilder builder = new ProdutoBuilder(); 
            builder.ComCodigo("1")
                .ComDescricao("Arroz")
                .ComPreco(5)
                .ComUnidade("KG").Finalizar();
            Lista.Add(new Produto(builder));
        }
        public IImmutableList<Produto> Obter(IImmutableList<string> codigos)
        {
            IQueryable<Produto> produtosEncontrados = Lista.AsQueryable();

            foreach (var codigo in codigos)
            {
                produtosEncontrados = produtosEncontrados.Where(e => e.Codigo == codigo);
            }

            return produtosEncontrados.ToImmutableList();
        }
    }
}