using System;
namespace Domain.Produtos
{
    public class ProdutoException : Exception
    {



        public ProdutoException(string codigo, CodigoErro codigoErro):
            base(codigo+" - "+ (codigoErro == CodigoErro.ProdutoNaoEncontrado))
        {
      
        }
    }
}