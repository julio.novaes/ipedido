using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Domain.Produtos
{
    public sealed class Produto
    {
        public readonly string Descricao, Unidade, Codigo;

        public readonly double Preco;

        internal Produto(ProdutoBuilder p) {
            Descricao = p.Descricao;
            Unidade = p.Unidade;
            Preco = p.Preco;
            Codigo = p.Codigo;
        }

     

        public static ProdutoBuilder Criar() {
            return new ProdutoBuilder();
        }


        public static IImmutableList<Produto> Obter(IImmutableList<string> codigos, IObterProduto obter)
        {
            IImmutableList<Produto> immutableList = obter.Obter(codigos);
            if (immutableList.Count == 0) throw new ProdutoException("", CodigoErro.ProdutoNaoEncontrado);
            List<string> listaCodigo = new List<string>();
            foreach (var produto in immutableList)
            {
                listaCodigo.Add(produto.Codigo);
            }

            var enumerable = listaCodigo.Except(codigos).ToArray();
            
            if (enumerable.Length > 0)
            {
                foreach (var codigo in enumerable)
                {
                    throw new ProdutoException("Produto nao encontrado: " + codigo, CodigoErro.ProdutoNaoEncontrado);
                }
            }
            return immutableList;
        }
        
        

    }

    public class ProdutoBuilder {

        protected internal string Descricao {get; private set;} 
        protected internal string Codigo {get; private set;} 
        protected internal string Unidade {get; private set;}
        protected internal double Preco {get; private set;}

        public ProdutoBuilder()
        {
            
        }

        public ProdutoBuilder(Produto produto)
        {
            Descricao = produto.Descricao;
            Codigo = produto.Codigo;
            Unidade = produto.Unidade;
            Preco = produto.Preco;
        }
        
        public ProdutoBuilder ComDescricao(string descricao) {
            Descricao = descricao;
            return this;
        }
        
        public ProdutoBuilder ComCodigo(string codigo) {
            Codigo = codigo;
            return this;
        }


        public ProdutoBuilder ComUnidade(string unidade){
            Unidade = unidade;
            return this;
        }

        public ProdutoBuilder ComPreco(double preco) {
            Preco = preco;
            return this;
        }

        public Produto Finalizar() {
            return new Produto(this);
        }
    } 
}