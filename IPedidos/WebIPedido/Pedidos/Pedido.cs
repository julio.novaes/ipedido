using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using WebIPedido.Clientes;
using WebIPedido.FormasPagamento;
using WebIPedido.Produtos;
using WebIPedido.Vendedores;

namespace WebIPedido.Pedidos
{
    public class Pedido
    {
         public int Id {get; set; }
         
         [ForeignKey("Cliente")]
         public int IdCliente { get; set; }
         //public virtual Cliente Cliente { get; set; }
         
         [ForeignKey("Vendedor")]
         public int IdVendedor { get; set; }
         //public virtual Vendedor Vendedor { get; set; }
         
         [ForeignKey("FormaPagamento")]
         public int IdFormaPagamento { get; set; }
         //public virtual FormaPagamento FormaPagamento { get; set; }

         public virtual List<ItemPedido> ItensPedido { get; set; }
         
    }
}