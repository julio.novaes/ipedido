using System.ComponentModel.DataAnnotations.Schema;

namespace WebIPedido.Pedidos
{
    public class ItemPedido
    {
        public int Id { get; set; }
        
        [ForeignKey("Pedido")]
        //public virtual Pedido Pedido { get; set; }
        //public int IdPedido { get; set; }

        public double Quantidade { get; set; }
        public double PrecoVenda { get; set; }
        
    }
}