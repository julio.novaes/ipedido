using System.Linq;
using Domain.Clientes;

namespace WebIPedido.Clientes
{
    public class ObterCliente:IObterCliente
    {
        private PedidoDBContext ctx;

        public ObterCliente(PedidoDBContext ctx)
        {
            this.ctx = ctx;
        }


        public Cliente Obter(string numeroContribuinte)
        {
            return ctx.Clientes.FirstOrDefault(banco => banco.NumeroContribuinte == numeroContribuinte)?.ParaCliente();

        }
    }
}