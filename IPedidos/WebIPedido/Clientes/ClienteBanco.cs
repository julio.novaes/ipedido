using Domain;
using Domain.Clientes;

namespace WebIPedido.Clientes
{
    public class ClienteBanco
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string NumeroContribuinte { get; set; }
        public string Cidade { get; set; }

        public Cliente ParaCliente()
        {
            return new ClienteBuilder()
                .ComCidade(Cidade)
                .ComNome(Nome)
                .ComNumeroContribuinte(NumeroContribuinte)
                .Criar();
        }
    }
    
    
}