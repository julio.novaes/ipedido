using Microsoft.EntityFrameworkCore;
using WebIPedido.Clientes;
using WebIPedido.FormasPagamento;
using WebIPedido.Pedidos;
using WebIPedido.Produtos;
using WebIPedido.Vendedores;

namespace WebIPedido
{
    public class PedidoDBContext : DbContext
    {
        public DbSet<ClienteBanco> Clientes { get; set; }
        public DbSet<FormaPagamento> FormasPagamento { get; set; }
        public DbSet<Pedido> Pedidos { get; set; }
        public DbSet<ItemPedido> ItensPedido { get; set; }
        public DbSet<Produto> Produtos { get; set; }

        public PedidoDBContext(DbContextOptions<PedidoDBContext> options) : base(options)
        {
              
        }
    }
}