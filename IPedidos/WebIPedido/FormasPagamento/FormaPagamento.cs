namespace WebIPedido.FormasPagamento
{
    public class FormaPagamento
    {
        public int Id { get; set; }
        public string Descricao { get; set; }

        public Domain.FormaPagamentos.FormaPagamento ParaFormaPagamento()
        {
            return Domain.FormaPagamentos.FormaPagamento.NovaFormaPagamento(Descricao);

        }
    }
}