using System.Linq;
using Domain.FormaPagamentos;

namespace WebIPedido.FormasPagamento
{
    public class ObterFormaPagamento : IObterFormaPagamento
    {
        private PedidoDBContext ctx;

        public ObterFormaPagamento(PedidoDBContext ctx)
        {
            this.ctx = ctx;
        }

        public Domain.FormaPagamentos.FormaPagamento Obter(string descricao)
        {
            var paraFormaPagamento = ctx.FormasPagamento.FirstOrDefault(e => e.Descricao.Equals(descricao))?
                .ParaFormaPagamento();
            return paraFormaPagamento; 
        }
    }
}