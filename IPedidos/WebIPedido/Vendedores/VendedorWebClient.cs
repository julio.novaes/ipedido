using System.Runtime.InteropServices;
using Domain.Vendedores;

namespace WebIPedido.Vendedores
{
    public class VendedorWebClient
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public Vendedor ParaVendedor()
        {
            return Vendedor.ComNome(Nome);
        }

        public override string ToString()
        {
            return Nome;
        }
    }
}