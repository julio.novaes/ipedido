using Domain.Vendedores;
using WebIPedido.ClienteRest;

namespace WebIPedido.Vendedores
{
    public class ObterVendedor : IObterVendedor
    {
        private readonly IClienteRest rest;

        public ObterVendedor(IClienteRest rest)
        {
            this.rest = rest;
        }

        public Vendedor Obter(string Nome)
        {
            return rest.ObterWeb(Nome).ParaVendedor();
        }
    }
}