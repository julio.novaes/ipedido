using Domain.FormaPagamentos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebIPedido.Pedidos;

namespace WebIPedido
{
    [Route("")]
    [ApiController]
    public class PedidoController : Controller
    {
        private IObterFormaPagamento obterFormaPagamento;
        private readonly ILogger<PedidoController> _logger;

        public PedidoController(IObterFormaPagamento obterFormaPagamento, ILogger<PedidoController> logger)
        {
            this.obterFormaPagamento = obterFormaPagamento;
            _logger = logger;
        }

        [HttpPost]
        public IActionResult PostPedido(Pedido Pedido)
        {
            var formaPagamento = obterFormaPagamento.Obter("A prazo");
            _logger.LogInformation(formaPagamento.ToString());
            return Ok();
        }
    }
}