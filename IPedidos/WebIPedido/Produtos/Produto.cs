namespace WebIPedido.Produtos
{
    public class Produto
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public string Unidade { get; set; }
        public double Preco { get; set; }
        
    }
}