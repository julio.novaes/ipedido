using System.Threading.Tasks;
using WebIPedido.Vendedores;

namespace WebIPedido.ClienteRest
{
    public interface IClienteRest
    {
        public VendedorWebClient ObterWeb(string nome);
    }
}