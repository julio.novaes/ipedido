using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using WebIPedido.Vendedores;

namespace WebIPedido.ClienteRest
{
    public class ClienteRestConcrete : IClienteRest
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        public ClienteRestConcrete(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _configuration = configuration;
        }
        public VendedorWebClient ObterWeb(string nome)
        {
            string url = _configuration["UrlVendedor"];
            var deserializeAsync = JsonSerializer.DeserializeAsync<VendedorWebClient>(Conexao(new Uri(url + nome)).Result);
            return deserializeAsync.Result;
        }

        private Task<Stream> Conexao(Uri uri)
        {
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            Task<Stream> task = Task.FromResult(_httpClient.GetStreamAsync(uri).Result);
            return task;
        }
    }
}