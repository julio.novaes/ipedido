using System.Collections.Generic;
using System.Collections.Immutable;
using Xunit;
using Domain.Produtos;

namespace Domain.tests
{
    public class ProdutoTeste
    {
        [Fact]
        public void TesteCriarProduto()
        {
            MockProdutos mock = new MockProdutos();
            List<string> lista = new List<string>()
            {
                "1"
            };
            ProdutoBuilder produto = new ProdutoBuilder();
            var immutableList = Produto.Obter(lista.ToImmutableList(), mock);
            

            Assert.True(immutableList.Count>0);
   
        }
        
        [Fact]
        public void TesteCriarProdutoFalhar()
        {
            MockProdutos mock = new MockProdutos();
            List<string> lista = new List<string>()
            {
                "2"
            };
            ProdutoBuilder produto = new ProdutoBuilder();
            
            Assert.Throws<ProdutoException>(() => Produto.Obter(lista.ToImmutableList(), mock));
        }
    }
}