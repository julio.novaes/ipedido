using Domain.Clientes;
using Xunit;

namespace Domain.tests
{
    
    public class ClienteTeste
    {
        [Fact]
        public void CriandoClienteValido()
        {
            ClienteBuilder c = new ClienteBuilder();
            IObterCliente obterCliente = new MockCliente();
            Cliente cliente = c.ComNome("Teste")
                .ComNumeroContribuinte("123456")
                .ComCidade("Irece") 
                .Criar();

            Assert.Equal("123456",cliente.NumeroContribuinte);
        }

        [Fact(DisplayName = "Teste de obtenção de cliente com numero nulo.")]
        public void ObtendoClienteNumeroContribuinteNulo()
        {
            IObterCliente obterCliente = new MockCliente();
            ClienteBuilder cliente = new ClienteBuilder();
            Assert.Throws<NumeroContribuinteNuloException>(() => cliente.ComNumeroContribuinte(null).Criar().Obter(obterCliente));
        }
        
        [Fact(DisplayName = "Teste de obtenção de cliente com numero invalido.")]
        public void ObtendoClienteNumeroContribuinteInvalido()
        {
            IObterCliente obterCliente = new MockCliente();
            ClienteBuilder cliente = new ClienteBuilder();
            Assert.Throws<ClienteException>(() => cliente.ComNumeroContribuinte("010.010.010-00").Criar().Obter(obterCliente));
        }
        
        [Fact(DisplayName = "Teste de obtenção de cliente válido.")]
        public void ObtendoClienteNumeroContribuinteValido()
        {
            ClienteBuilder c = new ClienteBuilder();
            IObterCliente obterCliente = new MockCliente();
            Assert.Equal("274.417.270-73",c.ComNumeroContribuinte("274.417.270-73")
                                                         .Criar()
                                                         .Obter(obterCliente)
                                                         .NumeroContribuinte);
        }
        
    
    }
}