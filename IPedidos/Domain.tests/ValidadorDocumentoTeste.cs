using System;
using System.Collections.Generic;
using Domain.Clientes;
using Xunit;
namespace Domain.tests
{
    public class ValidadorDocumentoTeste
    {
        [Fact]
        public void TesteDocumentoNulo()
        {
            ValidadorDocumento validador = new CNPJ(null);
            Assert.False(validador.Validar());
        }

        [Fact]
        public void SeValidaExpressao()
        {
            ValidadorDocumento validador = new CNPJ("00.000.000/0000-00");
            Assert.True(validador.Validar());
        }

        [Fact]
        public void NumeroComLetra()
        {
            ValidadorDocumento validador = new CNPJ("A0.000.000/0000-00");
            Assert.False(validador.Validar());
        }
        
        [Fact]
        public void NumeroComDigitoVerificadorFalse()
        {
            ValidadorDocumento validador = new CNPJ("11.222.333/0001-91");
            Assert.False(validador.Validar());
        }
        
        [Fact]
        public void NumeroComDigitoVerificadorTrue()
        {
            ValidadorDocumento validador = new CNPJ("11.222.333/0001-81");
            Assert.True(validador.Validar());
        }
        
        [Fact]
        public void TesteDocumentoCPFNulo()
        {
            ValidadorDocumento validador = new CPF(null);
            Assert.False(validador.Validar());
        }
        [Fact]
        public void SeValidaCPFExpressao()
        {
            ValidadorDocumento validador = new CPF("000.000.000-00");
            Assert.True(validador.Validar());
        }
        
        [Fact]
        public void NumeroCPFComLetra()
        {
            ValidadorDocumento validador = new CPF("A00.000.000-00");
            Assert.False(validador.Validar());
        }
        
        [Fact]
        public void NumeroCPFComDigitoVerificadorFalse()
        {
            ValidadorDocumento validador = new CPF("111.444.777-15");
            Assert.False(validador.Validar());
        }
        
        [Fact]
        public void NumeroCPFComDigitoVerificadorTrue()
        {
            ValidadorDocumento validador = new CPF("111.444.777-35");
            Assert.True(validador.Validar());
        }
        
    }
}