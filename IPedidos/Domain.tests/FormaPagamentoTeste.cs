using Xunit;
using Domain.FormaPagamentos;

namespace Domain.tests
{
    public class FormaPagamentoTeste
    {
        [Fact]
        public void TesteCriarFormaPagamento()
        {
            FormaPagamento forma = FormaPagamento.NovaFormaPagamento("A vista");
            Assert.Equal("A vista", forma.Nome);
        }
        
        [Fact]
        public void TesteObterFormaPagamentoValida()
        {
            IObterFormaPagamento obter = new MockPagamento();
            FormaPagamento forma = FormaPagamento.NovaFormaPagamento("A vista");
            var formaPagamento = forma.Obter(obter);
            Assert.True(formaPagamento.Equals(forma));
        }
        
        [Fact]
        public void TesteObterFormaPagamentoInvalido()
        {
            IObterFormaPagamento obter = new MockPagamento();
            FormaPagamento forma = FormaPagamento.NovaFormaPagamento("Pix");
            Assert.Throws<FormaPagamentoException>(() => forma.Obter(obter));
        }
    }
}