using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Domain.Clientes;
using Domain.FormaPagamentos;
using Domain.Pedidos;
using Domain.Produtos;
using Domain.Vendedores;
using Xunit;

namespace Domain.tests
{
    public class PedidoTeste
    {
        [Fact]
        void SalvarPedidoValido()
        {
            List<string> lista = new List<string>() { "1" };
            MockPedido mockPedido = new MockPedido();
            Pedido pedido = Pedido.CriarInstancia()
                .ComCliente(new MockCliente().Obter("274.417.270-73"))
                .ComPagamento(new MockPagamento().Obter("A vista"))
                .ComProdutos(new MockProdutos().Obter(lista.ToImmutableList()))
                .ComVendedor(new MockVendedor().Obter("Mario"))
                .Criar();
            pedido.Salvar(mockPedido);
            Assert.Equal(pedido, mockPedido.Get().Select(e => e).FirstOrDefault());
        } 

    }
}