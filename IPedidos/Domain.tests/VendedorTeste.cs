using Xunit;
using Domain.Vendedores;

namespace Domain.tests
{
    
    public class VendedorTeste
    {
        [Fact]
        public void CriandoVendedorValido()
        {
            Vendedor vendedor = Vendedor.ComNome("Fera");
            Assert.Equal("Fera",vendedor.Nome);
        }
        
        [Fact]
        public void TesteObterVendedorValido()
        {
            IObterVendedor obter = new MockVendedor();
            Vendedor vendedor = Vendedor.ComNome("Mario");
            var novoVendedor = vendedor.Obter(obter);
            Assert.True(novoVendedor.Equals(vendedor));
        }
        
        [Fact]
        public void TesteObterVendedorInvalido()
        {
            IObterVendedor obter = new MockVendedor();
            Vendedor vendedor = Vendedor.ComNome("Fera");

            Assert.Throws<VendedorException>(() => vendedor.Obter(obter));
        }


    }
}