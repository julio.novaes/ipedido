using System.Collections.Immutable;
using Domain.Clientes;
using Domain.FormaPagamentos;
using Domain.Pedidos;
using Domain.Produtos;
using Domain.Vendedores;

namespace Aplicacao
{
    public class RealizarPedido : IRealizarPedido
    {
        private readonly IObterCliente _obterCliente;
        private readonly IObterFormaPagamento _obterFormaPagamento;
        private readonly IObterVendedor _obterVendedor;
        private readonly IObterProduto _obterProduto;
        private readonly ISalvarPedido _salvarPedido;

        public void EfetuarPedido(string cliente, string formaPagamento, string vendedor,
            IImmutableList<string> listaProdutos)
        {
            Pedido.CriarInstancia()
                .ComCliente(_obterCliente.Obter(cliente))
                .ComPagamento(_obterFormaPagamento.Obter(formaPagamento))
                .ComProdutos(_obterProduto.Obter(listaProdutos))
                .ComVendedor(_obterVendedor.Obter(vendedor)).Criar().Salvar(_salvarPedido);
        }

        public static RelizarPedioBuild Iniciar()
        {
            return new RelizarPedioBuild();
        }

        public RealizarPedido(RelizarPedioBuild build)
        {
            _obterCliente = build.ObterCliente;
            _obterFormaPagamento = build.ObterFormaPagamento;
            _obterVendedor = build.ObterVendedor;
            _obterProduto = build.ObterProduto;
            _salvarPedido = build.SalvarPedido;
        }

        public class RelizarPedioBuild
        {
            internal IObterCliente ObterCliente { get; private set; }
            internal IObterFormaPagamento ObterFormaPagamento { get; private set; }
            internal IObterVendedor ObterVendedor { get; private set; }
            internal IObterProduto ObterProduto { get; private set; }
            internal ISalvarPedido SalvarPedido { get; private set; }


            public RelizarPedioBuild()
            {
            }


            public RelizarPedioBuild comObterCliente(IObterCliente obterCliente)
            {
                ObterCliente = obterCliente;
                return this;
            }
            
            public RelizarPedioBuild comObterFormaPagamento(IObterFormaPagamento obterFormaPagamento)
            {
                ObterFormaPagamento = obterFormaPagamento;
                return this;
            }
            
            public RelizarPedioBuild comObterVendedor(IObterVendedor obterVendedor)
            {
                ObterVendedor = obterVendedor;
                return this;
            }
            
            public RelizarPedioBuild comObterProduto(IObterProduto obterProduto)
            {
                ObterProduto = obterProduto;
                return this;
            }
            
            public RelizarPedioBuild comSalvarPedido(ISalvarPedido salvarPedido)
            {
                SalvarPedido = salvarPedido;
                return this;
            }


            public RealizarPedido Criar()
            {
                return new RealizarPedido(this);
            }
        }
    }
}