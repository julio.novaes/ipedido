﻿using System.Collections.Immutable;

namespace Aplicacao
{
    public interface IRealizarPedido
    {
        public void EfetuarPedido
                    (string cliente, 
                    string formaPagamento, 
                    string vendedor, 
                    IImmutableList<string> listaProdutos);
    }
}