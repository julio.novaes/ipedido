using System.Collections.Generic;
using System.Collections.Immutable;
using Domain.Clientes;
using Domain.FormaPagamentos;
using Domain.Pedidos;
using Domain.Produtos;
using Domain.Vendedores;
using Xunit;

namespace Aplicacao.Test
{
    public class UnitTest1
    {
        [Fact]
        public void SeRealizarPedidoValido()
        {
            List<string> lista = new List<string>() { "1" };    
            MockPedido salvarPedido = new MockPedido();
            IRealizarPedido pedido = RealizarPedido.Iniciar()
                .comObterCliente(new MockCliente())
                .comObterProduto( new MockProdutos())
                .comObterVendedor(new MockVendedor())
                .comObterFormaPagamento(new MockPagamento())
                .comSalvarPedido(salvarPedido).Criar();
            
            pedido.EfetuarPedido("274.417.270-73","A vista","Mario",lista.ToImmutableList());
            Assert.True(salvarPedido.Get().Count != 0);
        }
    }
}